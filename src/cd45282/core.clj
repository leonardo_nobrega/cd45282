(ns cd45282.core
  (:require [sparkling.conf :as conf]
            [sparkling.core :as spark]
            [taoensso.timbre :as log]))

;; option that looks like a solution but doesn't solve the problem:
;; "spark.port.maxRetries" "1"

;; experiment steps:
;;
;; term 1 cd45282/docker> ./start-nodes.sh
;; term 2 cd45282> ./build-and-start-repl.sh
;;
;; this will print a result (assuming the port is unblocked)
;; term 2 repl> (calculate-sum-with-timeout 20)
;; the spark context seems to cache the job result, so we stop it
;; term 2 repl> (stop-spark-context!)
;;
;; term 1 cd45282/docker> ./block-port.sh
;;
;; this will not print a result; the watchdog will stop the spark context
;; open the spark console at http://localhost:9080/ to see the connection failure logs
;; term 2 repl> (calculate-sum-with-timeout 60)
;;
;; term 1 cd45282/docker> ./unblock-port.sh
;;
;; this will print a result:
;; term 2 repl> (calculate-sum-with-timeout 20)
;; term 2 repl> exit
;;
;; term 1 cd45282/docker> ./stop-nodes.sh

(def config
  {:master "spark://172.17.0.1:7077"
   :app "cd45282"
   :jar (str "/Users/leonardo.nobrega/Documents/code/cd45282/target/"
             "cd45282-0.1.0-SNAPSHOT-standalone.jar")
   :options {"spark.driver.cores" "1"
             "spark.driver.memory" "256m"
             "spark.executor.memory" "512m"
             "spark.cores.max" "2"
             "spark.driver.port" "36053"}})

(defn apply-options
  [conf options]
  (reduce-kv conf/set conf options))

(def spark-context (atom nil))

(defn start-spark-context!
  []
  (locking spark-context
    (when-not @spark-context
      (-> (conf/spark-conf)
          (conf/master (:master config))
          (conf/app-name (:app config))
          (conf/jars (vector (:jar config)))
          (apply-options (:options config))
          spark/spark-context
          (#(reset! spark-context %))))))

(defn stop-spark-context!
  []
  (locking spark-context
    (when @spark-context
      (-> spark-context deref .stop)
      (reset! spark-context nil))))

(defn calculate-sum
  "Sums the numbers from 1 to 10."
  ([]
   (start-spark-context!)
   (calculate-sum @spark-context))

  ([sc]
   (->> 10
        range
        (map inc)
        (spark/parallelize sc)
        (spark/reduce +))))

(defn- run-job-with-timeout
  "Calls job then starts a thread that
  waits timeout-s seconds then calls stop-job."
  [job stop-job timeout-s]
  (let [state (atom :idle)
        job-start! (fn [] (locking state
                            (log/info "job starting")
                            (reset! state :job-running)))
        job-running? (fn [] (locking state
                              (-> state deref (= :job-running))))
        default-log (fn [new-state] (log/info "job done, state is" new-state))
        job-done! (fn [new-state & {:keys [log action]
                                    :or {log default-log
                                         action (constantly nil)}}]
                    (locking state
                      (when (job-running?)
                        (log new-state)
                        (reset! state new-state)
                        (action))))]

    ;; timer thread
    (future
      (log/info "starting timer - waiting for" timeout-s "seconds")
      (Thread/sleep (* 1000 timeout-s))
      (job-done! :timer-expired :action stop-job))

    ;; job
    (job-start!)
    (let [result (try (job)
                      (catch Exception e
                        (job-done! :job-failed
                                   :log (fn [_] (log/info e "job failed!")))))]
      (job-done! :job-complete)
      {:state (deref state)
       :result result})))

(defn calculate-sum-with-timeout
  [timeout-s]
  (run-job-with-timeout (fn [] (calculate-sum))
                        stop-spark-context!
                        timeout-s))
