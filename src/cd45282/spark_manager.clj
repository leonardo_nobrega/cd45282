(ns cd45282.spark-manager
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]))

(def log4j-zk-path "/cenx/config/analytics/log4j")

(definterface Set
  (set [this _]))

(defn return-spark-conf
  "stub for cenx.plataea.util/return-spark-conf"
  []
  (proxy [Set] [] (set [_] nil)))

(defn get-param
  "stub for cenx.plataea.config/get-param"
  [_]
  "")

(defn create-spark-context
  "stub for cenx.prometheus.spark.context/create-spark-context"
  [_]
  nil)

(defn sanity-job!
  "stub for cenx.levski.spark-sanity/sanity-job!"
  [_]
  nil)

(defn get-config
  "stub for cenx.hades.configurator/get-config"
  [_]
  nil)

(defn load-from
  "stub for clojurewerkz.propertied.properties/load-from"
  [_]
  nil)

(defn configure
  "stub for org.apache.log4j.PropertyConfigurator/configure"
  [_]
  nil)

(definterface SparkMgr
  (startSparkContext [])
  (stopSparkContext []))

(defn make-spark-manager
  "This was an attempt to create a spark manager component with mutable state.

  A component in the context of levski is an object that implements the
  stuartsierra.component Lifecycle interface; an object that provides the methods
  start and stop.

  My intention was to have a mutable spark-context attribute and to add the methods
  start-spark-context and stop-spark-context. The start-spark-context method would
  execute the sanity job. The stop method would be used for testing the start method.
  Since the proxy below implements the same interfaces as an immutable component,
  no changes would be required to use it.

  It seems though that the proxy created here is discarded by the stuartsierra
  component machinery. When I inspected the spark-manager on the repl, I found it to
  be a PersistentArrayMap object."

  [analytics-config]
  (let [mutable-state (atom {:spark-context nil
                             :sanity-state :not-done
                             :meta nil})
        update! (fn [k v] (swap! mutable-state #(assoc % k v)))
        get (fn [k] (-> mutable-state deref k))

        idle? (fn [] (let [sc (get :spark-context)]
                       (or (nil? sc)
                           (-> sc .sc .isStopped))))
        running? (fn [] (not (idle?)))

        start-spark-context
        (fn [] (if (idle?)
                 (let [sconf (doto (return-spark-conf)
                               (.set "spark.scheduler.mode" "FAIR")
                               (.set "spark.scheduler.allocation.file"
                                     (get-param :spark-allocation-file)))
                       sc (create-spark-context sconf)
                       sanity-state (-> sc sanity-job! :state)]
                   (update! :spark-context sc)
                   (update! :sanity-state sanity-state))
                 (log/info "spark-context is running")))

        stop-spark-context
        (fn [] (if (running?)
                 (-> :spark-context get .stop)
                 (log/info "spark-context is not running")))]

    (proxy [cd45282.spark_manager.SparkMgr
            com.stuartsierra.component.Lifecycle

            clojure.lang.ILookup
            clojure.lang.IObj
            clojure.lang.IMeta
            clojure.lang.Associative
            clojure.lang.IPersistentCollection
            clojure.lang.Seqable] []

      ;; SparkMgr
      (startSparkContext []
        (start-spark-context))
      (stopSparkContext []
        (stop-spark-context))

      ;; Lifecycle
      (start []
        (log/info "Starting up the SparkManager.")
        (log/info "Changing the propertyconfigurator logging info.")
        (-> log4j-zk-path
            get-config
            load-from
            configure)
        (start-spark-context))
      (stop []
        (log/info "Stopping the spark context.")
        (stop-spark-context))

      ;; all methods below delegate to the map inside the mutable-state atom

      ;; ILookup
      (valAt [k]
        (-> mutable-state deref (.valAt k)))

      ;; IObj
      (withMeta [meta]
        (-> mutable-state deref (.withMeta meta))
        this)

      ;; IMeta
      (meta []
        (-> mutable-state deref .meta))

      ;; Associative
      (containsKey [k]
        (-> mutable-state deref (.containsKey k)))
      (entryAt [k]
        (-> mutable-state deref (.entryAt k)))
      (assoc [k v]
        (-> mutable-state deref (.assoc k v))
        this)

      ;; IPersistentCollection
      (count []
        (-> mutable-state deref .count))
      (cons [obj]
        (-> mutable-state deref (.cons obj))
        this)
      (empty []
        (-> mutable-state deref .empty)
        this)
      (equiv [obj]
        (-> mutable-state deref (.equiv obj)))

      ;; Seqable
      (seq []
        (-> mutable-state deref .seq)))))
