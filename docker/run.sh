#!/bin/bash

set -e

# configure log rotation and curation
# https://spark.apache.org/docs/2.0.2/configuration.html#configuring-logging
export MAX_FILE_SIZE=${MAX_FILE_SIZE:-100MB}
export MAX_BACKUP_INDEX=${MAX_BACKUP_INDEX:-2}
sed -ri "s#\{\{MAX_FILE_SIZE\}\}#$MAX_FILE_SIZE#" "$SPARK_HOME/conf/log4j.properties"
sed -ri "s#\{\{MAX_BACKUP_INDEX\}\}#$MAX_BACKUP_INDEX#" "$SPARK_HOME/conf/log4j.properties"

# TODO need to configure memory and cores (default is all RAM minus 1G for OS and all cores)
#/usr/spark-2.0.2/conf/spark-defaults.conf
#SPARK_WORKER_CORES: 2
#SPARK_WORKER_MEMORY: 1g

# Hostname your Spark program will advertise to other machines
export SPARK_PUBLIC_DNS=${CONTAINER_HOST_ADDRESS}

# TODO configure ports
# https://cenx-cf.atlassian.net/wiki/display/DOCS/deploy.yaml+File+Reference
# https://spark.apache.org/docs/latest/security.html
# https://spark.apache.org/docs/latest/configuration.html#networking

# environment variables mentioned here:
# SPARK_MASTER
# SPARK_MASTER_SPARK_MASTER1_HOST
# SPARK_MASTER_SPARK_MASTER1_MASTER_PORT
# CONTAINER_HOST_ADDRESS

# https://stackoverflow.com/questions/5163144/what-are-the-special-dollar-sign-shell-variables
# if the number of arguments passed to this script is zero
if [ $# -eq 0 ]; then
    if [ "x${SPARK_MASTER}" != "x" ] && [ "${SPARK_MASTER}" == "true" ]; then
        exec bin/spark-class org.apache.spark.deploy.master.Master
    else
        exec bin/spark-class org.apache.spark.deploy.worker.Worker spark://${SPARK_MASTER_SPARK_MASTER1_HOST}:${SPARK_MASTER_SPARK_MASTER1_MASTER_PORT}
    fi
else
    # exec with the given parameters
    exec "$@"
fi
