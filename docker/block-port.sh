# source:
# https://superuser.com/questions/505128/deny-access-to-a-port-from-localhost-on-osx

sudo pfctl -s all > /dev/null
(sudo pfctl -sr 2>/dev/null; \
echo "block drop quick on lo0 proto tcp from any to any port = 36053") | \
sudo pfctl -e -f - 2>/dev/null
