(defproject cd45282 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [gorillalabs/sparkling "2.0.0" :exclusions [com.esotericsoftware.kryo/kryo]]
                 [org.apache.spark/spark-streaming_2.11 "2.0.2"]
                 [org.apache.spark/spark-core_2.11 "2.0.2"]
                 [org.apache.spark/spark-sql_2.11 "2.0.2"]
                 [org.scala-lang/scala-library "2.11.2"]
                 [com.stuartsierra/component "0.2.3"]
                 [com.taoensso/timbre "4.8.0"]]
  
  :profiles {;; with this cd45282.core will be loaded
             ;; on the repl created with C-c M-j
             :repl {:main cd45282.core
                    :target-path "target"}

             ;; use lein with-profile prod install to build the standalone jar
             ;; see https://github.com/pliant/lein-package
             :prod {:aot [#".*"
                          sparkling.serialization sparkling.destructuring]
                    :plugins [[lein-package "2.1.1"]]
                    :hooks [leiningen.package.hooks.deploy
                            leiningen.package.hooks.install]
                    :package {:skipjar false
                              :autobuild true
                              :reuse false
                              :artifacts [{:build "uberjar"
                                           :classifier "standalone"
                                           :extension "jar"}]}}})
